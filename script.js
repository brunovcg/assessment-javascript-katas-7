
// ---------------------------------------------------- KATA 1 - FOR.EACH --------------------------------------------------------------------

console.log("%cfunction newForEach","background-color:green ; color:white")

const kata1 = ["a", "b", "c", "d", "e", "g","h"]

function newForEach(array, callback) {

    for (let i = 0; i < array.length; i++) {
        const currentValue = array[i]
        callback(currentValue, i, array)
    }

    console.log(`%c newForEach to CONSOLE.LOG if each letter of this array ${JSON.stringify(array)} is consonant or vowel`, "color:red")
}

 newForEach(kata1, function (letter, index, array) {

    let letterType = "";

    if(letter === "a" || letter === "e" || letter === "i" || letter === "o" || letter === "u") {letterType = "vowel"
        } else {letterType = "consonant"}  

    console.log(`The letter "${letter}" at position "${index}" of ${JSON.stringify(array)} is a ${letterType}.`)
})

console.log(kata1.forEach(function (letter, index, array) {

    let letterType = "";

    if(letter === "a" || letter === "e" || letter === "i" || letter === "o" || letter === "u") {letterType = "vowel"
        } else {letterType = "consonant"}  

    console.log(`%c using .forEach: The letter "${letter}" at position "${index}" of ${JSON.stringify(array)} is a ${letterType}.`,"color:blue")
}))

// ---------------------------------------------------- KATA 2 - MAP --------------------------------------------------------------------------

console.log("%cfunction newMap","background-color:green ; color:white")


const kata2 = [1,2,3,4,5,6]
let kata2output = []

function newMap(array, callback) {

    for (let i = 0; i < array.length; i++) {
        const currentValue = array[i]
        callback(currentValue, i, array)
    }

    console.log(`%c newMap to square the numbers of this array ${JSON.stringify(array)} it create a new one ${JSON.stringify(kata2output)}`, "color:red")

    return kata2output
}

newMap(kata2, function (number, index, array) {

    let squared = number*number 
    kata2output.push(squared)
    
    console.log(`The number "${number}" at position "${index}" of ${JSON.stringify(array)} squared is "${squared}".`)

})


console.log(`%c using .map: ${JSON.stringify(kata2.map(x => x*x))}`, "color:blue")

// ---------------------------------------------------- KATA 3 - SOME -------------------------------------------------------------------------

console.log("%cfunction newSome","background-color:green ; color:white")

const kata3 = [2,1,6,8]
let kata3TestOdd = 0

function newSome(array, callback) {

    let kata3output = ""

    for (let i = 0; i < array.length; i++) {
        const currentValue = array[i]
        callback(currentValue, i, array)

    }

    if (kata3TestOdd > 0) {kata3output = true} else {kata3output = false}

    console.log(`%c newSome to check if at least one number is odd in array ${JSON.stringify(array)} it RETURN: ${kata3output}`, "color:red")

    return kata3output
}


newSome(kata3, function (number, index, array) {
    
    let isOdd = ""
   
    if(number % 2 != 0) {
        isOdd = true
        kata3TestOdd ++

        console.log(`The number "${number}" at position "${index}" of ${JSON.stringify(array)} is Odd? ${isOdd}.`)


    } else {
        isOdd = false
    
        console.log(`The number "${number}" at position "${index}" of ${JSON.stringify(array)} is Odd? ${isOdd}.`)
    }  

    
})


console.log(`%c using .some: ${kata3.some(x => x % 2 != 0)}`, "color:blue")


// ---------------------------------------------------- KATA 4 - FIND -------------------------------------------------------------------------

console.log("%cfunction newFind","background-color:green ; color:white")

const kata4 = [1, 3, 3, 7, 5, 6]
let kata4output = ""

function newFind(array, callback) {


    for (let i = 0; i < array.length; i++) {
        const currentValue = array[i]
        callback(currentValue, i, array)

        if(currentValue>5){break}
    }

    console.log(`%c newFind on ${JSON.stringify(array)} finds the first element that is true for condition, first element is ${JSON.stringify(kata4output)}`, "color:red")
}

newFind(kata4, function (number, index, array) {
    
    let isGreaterThan5 = ""
   
    if(number>5) {
        kata4output = number
       isGreaterThan5 = true


    } else {
        isGreaterThan5 = false  
    }

    console.log(`The number "${number}" at position "${index}" of ${JSON.stringify(array)} is > 5? ${isGreaterThan5}.`)  

})

console.log(`%c using .find: ${kata4.find(x => x > 5)}`, "color:blue")


// ---------------------------------------------------- KATA 5 - FIND.INDEX ---------------------------------------------------------------------

console.log("%cfunction newFindIndex","background-color:green ; color:white")


const kata5 = [1, 3, 3, 7, 5, 6]
let kata5output = ""

function newFindIndex(array, callback) {


    for (let i = 0; i < array.length; i++) {
        const currentValue = array[i]
        callback(currentValue, i, array)

        if(currentValue>5){break}
    }

    if (kata5output == "" ) {kata5output = -1}

    console.log(`%c newFindIndex on ${JSON.stringify(array)} finds the index of the first element that is true for condition, it's in position "${kata5output}"`, "color:red")

    return kata5output
}

newFindIndex(kata5, function (number, index, array) {
    
    let isGreaterThan5 = ""
   
    if(number>5) {
        kata5output = index
       isGreaterThan5 = true


    } else {
        isGreaterThan5 = false  
    }

    console.log(`The number "${number}" at position "${index}" of ${JSON.stringify(array)} is > 5? ${isGreaterThan5}.`)

})

console.log(`%c using .findIndex: "${kata5.findIndex(x => x > 5)}"`, "color:blue")

// ---------------------------------------------------- KATA 6 - EVERY --------------------------------------------------------------------------

console.log("%cfunction newEvery","background-color:green ; color:white")


const kata6 = [1,3,5,9,-1]
let kata6greaterThan0 = 0

function newEvery(array, callback) {

    let kata6output = ""

    for (let i = 0; i < array.length; i++) {
        const currentValue = array[i]
        callback(currentValue, i, array)
    }

    if (kata6greaterThan0 > 0) {kata6output = false} else {kata6output = true}

    console.log(`%c newEvery to check if all the numbers are > 0 in ${JSON.stringify(array)} it RETURN: ${kata6output}`, "color:red")

    return kata6output
}


newEvery(kata6, function (number, index, array) {
    
    let isTrue = ""
   
    if(number > 0) {
        isTrue = true

    } else {
        kata6greaterThan0 ++
        isTrue = false}  

    console.log(`The number "${number}" at position "${index}" of ${JSON.stringify(array)} is > 0 ? ${isTrue}.`)
})

console.log(`%c using .every: ${kata6.every(x => x > 0)}`, "color:blue")


// ---------------------------------------------------- KATA 7 - FILTER --------------------------------------------------------------------------


console.log("%cfunction newFilter","background-color:green ; color:white")


const kata7 = [-1,2,0,-10,5,6]
let kata7output = []

function newFilter(array, callback) {

    for (let i = 0; i < array.length; i++) {
        const currentValue = array[i]
        callback(currentValue, i, array)
    }

    console.log(`%c newFilter on ${JSON.stringify(array)} creates a new one with the elements > 0 ${JSON.stringify(kata7output)}`, "color:red")

    return kata7output
}

newFilter(kata7, function (number, index, array) {

    let filterTest = ""
    if(number>0){
    kata7output.push(number)
    filterTest = true} else {filterTest = false}
    
    console.log(`The number "${number}" at position "${index}" of ${JSON.stringify(array)} is > 0? ${filterTest}`)

})

console.log(`%c using .filter: ${JSON.stringify(kata7.filter(x => x >0 ))}`, "color:blue")




// ---------------------------------------------------- BONUS 1 - CONCAT --------------------------------------------------------------------------
console.log("%cfunction newConcat","background-color:green ; color:white")

const bonus1Array1 = ["a","b","c"]
const bonus1Array2 = [1,2,3,4]

function newConcat (array1, array2) {

    let bonus1Output = []

    for (let i = 0; i < array1.length; i++){

        bonus1Output.push(array1[i])

        console.log(`The element "${array1[i]}" at position "${i}" of ${JSON.stringify(array1)} get pushed to the new array ${JSON.stringify(bonus1Output)}`)
    }

    for (let i = 0; i < array2.length; i++){

        bonus1Output.push(array2[i])

        console.log(`The element "${array2[i]}" at position "${i}" of ${JSON.stringify(array2)} get pushed to the new array ${JSON.stringify(bonus1Output)}`)
    }

    console.log(`%c newConcat for ${JSON.stringify(array1)} and ${JSON.stringify(array2)} it get a new array ${JSON.stringify(bonus1Output)}`, "color:red")
    return bonus1Output
}

newConcat(bonus1Array1,bonus1Array2)



console.log(`%c using .concat: ${JSON.stringify(bonus1Array1.concat(bonus1Array2))}`, "color:blue")

// ---------------------------------------------------- BONUS 2 - INCLUDES -------------------------------------------------------------------------

console.log("%cfunction newIncludes","background-color:green ; color:white")

const bonus2string = 2
const bonus2array = [1,2,3,4]

function newIncludes (array, string) {

    let bonus2Output = ""

    for (let i = 0; i < array.length; i++){


        if (string === array[i]) {
            bonus2Output = true
            console.log(`element "${array[i]}" in position "${[i]}" is included in ${JSON.stringify(array)}: ${bonus2Output}`)

            break
        } else {
            bonus2Output = false
            console.log(`element "${array[i]}" in position "${[i]}" is included in ${JSON.stringify(array)}: ${bonus2Output}`)
        }

    }

    console.log(`%c newIncludes check if array ${JSON.stringify(array)} includes as an element ${string} and RETURN: ${bonus2Output}`, "color:red")
    return bonus2Output
}

newIncludes(bonus2array,bonus2string)


console.log(`%c using .includes: ${bonus2array.includes(bonus2string)}`, "color:blue")



// ---------------------------------------------------- BONUS 3 - INDEX.OF --------------------------------------------------------------------------

console.log("%cfunction newIndexOf","background-color:green ; color:white")


const bonus3string = 2
const bonus3array = [1,3,3,4]

function newIndexOf (array, string) {

    let bonus3Output = ""
    let index = ""

    for (let i = 0; i < array.length; i++){


        if (string === array[i]) {
            bonus3Output = true
            index = i
            console.log(`element "${array[i]}" in position "${[i]}" is included in ${JSON.stringify(array)}: ${bonus3Output}`)

            break
        } else {
            bonus3Output = false
            console.log(`element "${array[i]}" in position "${[i]}" is included in ${JSON.stringify(array)}: ${bonus3Output}`)
        }

    }

    if (index == "") {index = -1}

    console.log(`%c. newIndexOf check if array ${JSON.stringify(array)} includes as an element "${string}" and RETURN its index: ${index}`, "color:red")
    return bonus3Output
}

newIndexOf(bonus3array,bonus3string)


console.log(`%c using .indexOf: ${bonus3array.indexOf(bonus3string)}`, "color:blue")



// ---------------------------------------------------- BONUS 4 - JOIN ------------------------------------------------------------------------------

console.log("%cfunction newJoin","background-color:green ; color:white")

const bonus4array = [1,3,3,4,5]
const bonus4separador = ","

function newJoin (array, separador) {

    let string = ""

    for (let i = 0; i < array.length; i++){

    string += `${array[i]}${separador}`

    console.log(`element "${array[i]}" in position "${i}" is joint to string "${string}" `)

    }

    string = string.slice(0,-1)

    console.log(`%c newJoin in array "${JSON.stringify(array)}" using separator "${separador}" return "${string}"`, "color:red")

    return string
    
}


newJoin (bonus4array, bonus4separador)


console.log(`%c using .join: "${bonus4array.join(bonus4separador)}"`, "color:blue")



// ---------------------------------------------------- BONUS 5 - SLICE -----------------------------------------------------------------------------


console.log("%cfunction newSlice","background-color:green ; color:white")

const bonus5array = [1,2,3,4,5,6,7,8,9,10]

let bonus5inicio = 5

let bonus5fim = 10

const bonus5intervalo = {inicio:bonus5inicio, fim: bonus5fim}

function newSlice (array, intervalo) {

    let seletorInicial = Object.values(intervalo)[0]
    let seletorFinal = Object.values(intervalo)[1]
    let bonus5Output = []

    if (seletorInicial > array.length) {console.log(JSON.stringify(bonus5Output)) ; return bonus5Output}

    if (seletorInicial === undefined && seletorFinal === undefined) {seletorInicial = 0 ; seletorFinal = array.length+1}

    else if (seletorInicial === undefined && seletorFinal > 0) {seletorInicial = 0 ; seletorFinal = seletorFinal}

    else if (seletorInicial === undefined && seletorFinal < 0) {seletorInicial = 0 ; seletorFinal = array.length + seletorFinal}

    else if (seletorInicial > 0 && seletorFinal === undefined) {seletorInicial = seletorInicial ; seletorFinal = array.length}

    else if (seletorInicial < 0 && seletorFinal === undefined) {seletorInicial = array.length + seletorInicial ; seletorFinal = array.length}

    else if (seletorInicial >= 0 && seletorFinal > 0) {seletorInicial = seletorInicial ; seletorFinal = seletorFinal} 

    else if (seletorInicial >= 0 && seletorFinal < 0) {seletorInicial = seletorInicial ; seletorFinal = array.length + seletorFinal + 1} 

    else if (seletorInicial < 0 && seletorFinal > 0) {seletorInicial = array.length + seletorInicial; seletorFinal = seletorFinal} 

    else {console.log(JSON.stringify(bonus5Output)) ; return bonus5Output}
      

for (let i = 0; i<array.length;i++) {


        if (i >= seletorInicial && i < seletorFinal) {
            
        bonus5Output.push(array[i])
       
        console.log(`element "${array[i]}" in position "${i}" of ${JSON.stringify(array)} corresponds rule: true`)
        
        } else {console.log(`element "${array[i]}" in position "${i}" of ${JSON.stringify(array)} corresponds rule: false`)}
}

console.log(`%c newSlice in array ${JSON.stringify(array)} with start argument "${seletorInicial}" e  final "${seletorFinal}" get array ${JSON.stringify(bonus5Output)}`, "color:red")
   
}

newSlice(bonus5array,bonus5intervalo)


console.log(`%c using .slice: ${JSON.stringify(bonus5array.slice(bonus5inicio,bonus5fim))}`, "color:blue")



// ---------------------------------------------------- BONUS 6 - FLAT -----------------------------------------------------------------------------

console.log("%cfunction newFlat","background-color:green ; color:white")

const bonus6array = [0,1,3,4,[1,3],4,5,[1,3,5,4,8]]

function newFlat(array) {

    let bonus6output = []


    
    for (let i = 0; i < array.length; i++) {

        if (Array.isArray(array[i])) {
            for(let j = 0; j < array[i].length; j++) {
                bonus6output.push(array[i][j])

                console.log(`Element "${array[i][j]}" in array ${JSON.stringify(array[i])} in array ${JSON.stringify(array)} is pushed into new one ${JSON.stringify(bonus6output)}`)
            }

        } else {bonus6output.push(array[i])
                console.log(`Element "${array[i]}" in array ${JSON.stringify(array)} is pushed into new one ${JSON.stringify(bonus6output)}`)
        }

    }

    console.log(`%c newFlat used to concat elements in array "${JSON.stringify(array)}" return new one ${JSON.stringify(bonus6output)}`, "color:red")

    return bonus6output
    
} newFlat(bonus6array)

console.log(`%c using .flat: ${JSON.stringify(bonus6array.flat())}`, "color:blue")


// ---------------------------------------------------- BONUS 7 - FLAT.MAP --------------------------------------------------------------------------


console.log("%cfunction newFlatMap","background-color:green ; color:white")

const bonus7array = [0,1,3,4,4,5]

function newFlatMap (array) {

    let bonus7output = []
    
    for (let i = 0; i < array.length; i++) {

        if (Array.isArray(array[i])) {
            for(let j = 0; j < array[i].length; j++) {
                bonus7output.push([array[i][j]*2])

                console.log(`Element "${array[i][j]}" in array ${JSON.stringify(array[i])} in array ${JSON.stringify(array)} has its double pushed into new one ${JSON.stringify(bonus7output)}`)
            }

        } else {bonus7output.push([array[i]*2])
                console.log(`Element "${array[i]}" in array ${JSON.stringify(array)} has its double pushed into new one ${JSON.stringify(bonus7output)}`)
        }

    }

    console.log(`%c newFlatMap used to concat the doubles of elements in array "${JSON.stringify(array)}" return new one ${JSON.stringify(bonus7output)}`, "color:red")

    return bonus7output
    
} newFlatMap(bonus7array)


console.log(`%c using .flatMap: ${JSON.stringify(bonus7array.flatMap(x=>[[x*2]]))}`, "color:blue")





// ---------------------------------------------------- BONUS 8 - ARRAY.OF ---------------------------------------------------------------------------


console.log("%cfunction newArrayOf","background-color:green ; color:white")

let bonus8string = "1,2,3"

function newArrayOf (string){ 

    let  testeArray= string.split(",")

    let bonus8output = []

 

for (let i = 0; i < testeArray.length;i++){

    if (isNaN(Number(testeArray[i]) === false)) {bonus8output.push((testeArray[i]))} else  (bonus8output.push(Number(testeArray[i])))
   
        console.log(`element "${testeArray[i]}" is pushed to new array ${JSON.stringify(bonus8output)}`)
  
}

console.log(`%c newArrayOf used to push elements "${string}" into array ${JSON.stringify(bonus8output)}`, "color:red")

return bonus8output
}

newArrayOf (bonus8string)

console.log(`%c using .Array.of: ${JSON.stringify(Array.of(1,2,3))}`, "color:blue")










